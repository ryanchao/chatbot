import jieba
import fileinput
import re
# $ pv data/grouped-sentences.txt | python jb_tokenize.py > jb-grouped-sentences.txt

jieba.set_dictionary('data/DICT_CK+jieba_lower')
jieba.load_userdict('data/zhwiki-zh-clean')

for line in fileinput.input():
    text = line.strip()
    if len(text) > 0:
        text = text.split('\t\t')[0].strip()
        print(re.sub(r'\s+', r' ', ' '.join(jieba.cut(text))))
    else: print()

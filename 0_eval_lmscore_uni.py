import kenlm
import fileinput
import re
MODEL = kenlm.Model('lm-experiment/model/uni-lm5.barpa')


for line in fileinput.input():
	context = re.sub(r'\s+',' ', re.sub(r'([^\u0021-\u007A])', r' \1 ', line)).strip()
	print('{}\t\t{:.1f}'.format(context, MODEL.score(context)))
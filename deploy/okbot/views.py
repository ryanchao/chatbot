from django.http import HttpResponse, HttpResponseForbidden
from django.views.decorators.csrf import csrf_exempt
import tensorflow as tf
from random import random, randint
import json
import requests
import logging
import jieba
from .seq2seq_decode import create_model, PARAMS
from . import data_utils
import os
import re
import numpy as np


EN_VOCAB, _ = data_utils.initialize_vocabulary(os.path.join(PARAMS['data_dir'], "vocab%d.en" % PARAMS['en_vocab_size']))
_, REV_FR_VOCAB = data_utils.initialize_vocabulary(os.path.join(PARAMS['data_dir'], "vocab%d.fr" % PARAMS['fr_vocab_size']))

SESS = tf.Session()
MODEL = create_model(SESS, True)


logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s: %(message)s', datefmt='[%d/%b/%Y %H:%M:%S]')

# Create your views here.
OKBOT_PAGE_ACCESS_KEY=os.environ['OKBOT_PAGE_ACCESS_KEY']
OKBOT_VERIFY_TOKEN=os.environ['OKBOT_VERIFY_TOKEN']

def index(request):
    # reply = bot_reply('你好') + '<br>'
    # reply += bot_reply('薪水多少') + '<br>'
    # reply += bot_reply('再見') + '<br>'
    # c = 0
    # with tf.Session() as sess:
    #     a = tf.constant(10 * random())
    #     b = tf.constant(10 * random())
    #     c = sess.run(a + b)
    # tf_msg = 'tensor result: ' + str(c)

    return HttpResponse()


@csrf_exempt
def fb_webhook(request):
    if request.method == 'GET':
        if request.GET.get("hub.mode") == "subscribe" and request.GET.get("hub.challenge"):
            if not request.GET.get("hub.verify_token") == OKBOT_VERIFY_TOKEN:
                return HttpResponseForbidden("Verification token mismatch")
            return HttpResponse(request.GET.get("hub.challenge"))
    elif request.method == 'POST':
        try:
            incoming = json.loads(request.body.decode('utf-8'))
        except Exception as e:
            logging.warning(e)
            return HttpResponse()
        # print(json.dumps(incoming, sort_keys=True, indent=4, separators=(',', ': ')))
        for entry in incoming['entry']:
            for message_evt in entry['messaging']:
                sender_id = message_evt.get('sender').get('id')
                if 'message' in message_evt:
                    handle_message(sender_id, message_evt.get('message'))
        
    return HttpResponse()


def handle_message(sender_id, msg):
    if 'text' in msg:
        params = {
            "access_token": OKBOT_PAGE_ACCESS_KEY
        }
        headers = {
            "Content-Type": "application/json"
        }

        reply = bot_reply(msg.get('text'))
        data = json.dumps({
            "recipient": {
                "id": sender_id
            },
            "message": {
                "text": reply
            }
        })
        r = requests.post("https://graph.facebook.com/v2.6/me/messages", params=params, headers=headers, data=data)
        if r.status_code != 200:
            print(r.status_code)
            print(r.text)


def bot_reply(query_sentence):
    global SESS, MODEL
    sentence = ' '.join(list(jieba.cut(query_sentence)))
    sentence = re.sub(r'\s+', ' ', sentence)
    logging.info(sentence)
    try:
        token_ids = data_utils.sentence_to_token_ids(sentence, EN_VOCAB)
    except Exception as e:
        token_ids = [data_utils.EOS_ID]
        logging.warning(e)

    bucket_id = len(PARAMS['buckets']) - 1
    for i, bucket in enumerate(PARAMS['buckets']):
        if bucket[0] >= len(token_ids):
            bucket_id = i
            break
        else:
            logging.warning("Sentence truncated: %s", sentence) 

    # Get a 1-element batch to feed the sentence to the model.
    encoder_inputs, decoder_inputs, target_weights = MODEL.get_batch({bucket_id: [(token_ids, [])]}, bucket_id)
      
    # Get output logits for the sentence.
    _, _, output_logits = MODEL.step(SESS, encoder_inputs, decoder_inputs,
                                       target_weights, bucket_id, True)
    # This is a greedy decoder - outputs are just argmaxes of output_logits.
    outputs = [int(np.argmax(logit, axis=1)) for logit in output_logits]
    # If there is an EOS symbol in outputs, cut them at that point.
    if data_utils.EOS_ID in outputs:
        outputs = outputs[:outputs.index(data_utils.EOS_ID)]
        # Print out French sentence corresponding to outputs.
    
    reply =  ''.join([tf.compat.as_str(REV_FR_VOCAB[output]) for output in outputs])
    reply = re.sub(r'_UNK', _sub_unk, reply)
    return reply


def _sub_unk(matchobj):
    return str(randint(5, 20))








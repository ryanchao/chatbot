import tensorflow as tf
from tensorflow.models.rnn.translate import seq2seq_model



PARAMS = {
    'buckets': [(5, 10), (10, 15), (20, 25), (40, 50)],
    'en_vocab_size': 160000,
    'fr_vocab_size': 160000,
    'size': 512,
    'num_layers': 3,
    'max_gradient_norm': 5.0,
    'batch_size': 1,
    'learning_rate': 0.5,
    'learning_rate_decay_factor': 0.99,
    'forward_only': True,
    'train_dir': 'okbot/data/checkpoint',
    'data_dir': 'okbot/data'
}





def decode():
  with tf.Session() as sess:
    # Create model and load parameters.
    model = create_model(sess, True)

    model.batch_size = 1  # We decode one sentence at a time.

    # Load vocabularies.
    en_vocab_path = os.path.join(FLAGS.data_dir,
                                 "vocab%d.en" % FLAGS.en_vocab_size)
    fr_vocab_path = os.path.join(FLAGS.data_dir,
                                 "vocab%d.fr" % FLAGS.fr_vocab_size)
    en_vocab, _ = data_utils.initialize_vocabulary(en_vocab_path)
    _, rev_fr_vocab = data_utils.initialize_vocabulary(fr_vocab_path)

    # Decode from standard input.
    sys.stdout.write("> ")
    sys.stdout.flush()
    sentence = sys.stdin.readline()
    while sentence:
      # Get token-ids for the input sentence.
      # token_ids = data_utils.sentence_to_token_ids(tf.compat.as_bytes(sentence), en_vocab)
      try:
        token_ids = data_utils.sentence_to_token_ids(sentence, en_vocab)
      except Exception as e:
        token_ids = [data_utils.EOS_ID]
        print(str(e))

      print(token_ids)
      # Which bucket does it belong to?
      bucket_id = len(_buckets) - 1
      for i, bucket in enumerate(_buckets):
        if bucket[0] >= len(token_ids):
          bucket_id = i
          break
      else:
        logging.warning("Sentence truncated: %s", sentence) 

      # Get a 1-element batch to feed the sentence to the model.
      encoder_inputs, decoder_inputs, target_weights = model.get_batch(
          {bucket_id: [(token_ids, [])]}, bucket_id)
      
      # Get output logits for the sentence.
      _, _, output_logits = model.step(sess, encoder_inputs, decoder_inputs,
                                       target_weights, bucket_id, True)
      # This is a greedy decoder - outputs are just argmaxes of output_logits.
      outputs = [int(np.argmax(logit, axis=1)) for logit in output_logits]
      # If there is an EOS symbol in outputs, cut them at that point.
      if data_utils.EOS_ID in outputs:
        outputs = outputs[:outputs.index(data_utils.EOS_ID)]
      # Print out French sentence corresponding to outputs.
      print(" ".join([tf.compat.as_str(rev_fr_vocab[output]) for output in outputs]))
      print("> ", end="")
      sys.stdout.flush()
      sentence = sys.stdin.readline()




def create_model(session, forward_only):
    """Create translation model and initialize or load parameters in session."""
    dtype = tf.float32
    model = seq2seq_model.Seq2SeqModel(
        PARAMS['en_vocab_size'],
        PARAMS['fr_vocab_size'],
        PARAMS['buckets'],
        PARAMS['size'],
        PARAMS['num_layers'],
        PARAMS['max_gradient_norm'],
        PARAMS['batch_size'],
        PARAMS['learning_rate'],
        PARAMS['learning_rate_decay_factor'],
        forward_only = PARAMS['forward_only'],
        dtype=dtype)
    ckpt = tf.train.get_checkpoint_state(PARAMS['train_dir'])
    if ckpt and tf.train.checkpoint_exists(ckpt.model_checkpoint_path):
        print("Reading model parameters from %s" % ckpt.model_checkpoint_path)
        model.saver.restore(session, ckpt.model_checkpoint_path)
        return model



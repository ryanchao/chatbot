from django.conf.urls import url

import okbot.views

urlpatterns = [
    url(r'^$', okbot.views.index, name='index'),
    url(r'^chatwithme/', okbot.views.fb_webhook, name='chatwithme')
]

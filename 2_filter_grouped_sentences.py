import fileinput
import re
last_sent = ''
newline = True
group_head = False
# $ pv data/lm-hscore-sentences.txt | parallel -j 3 --keep-order --pipe python filter_grouped_sentences.py > grouped-sentences.txt
for line in fileinput.input():
    curr_sen = line.strip()
    if len(last_sent) > 0:
        if len(curr_sen) > 0:
            if group_head:
                print(last_sent)
                group_head = False
            print(curr_sen)
            newline = True
        else:
            if newline:
                print()
                newline = False
        last_sent = curr_sen
    elif len(curr_sen) > 0:
        last_sent = curr_sen
        group_head = True
    else: continue

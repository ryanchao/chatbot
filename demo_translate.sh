python translate.py --data_dir data/temp/data --train_dir data/temp/checkpoint --size=512 --num_layers=3 --steps_per_checkpoint=50 --en_vocab_size=160000 --fr_vocab_size=160000
# python translate.py --decode --data_dir data/ --train_dir data/checkpoint --size=512 --num_layers=3 --en_vocab_size=160000 --fr_vocab_size=160000

import kenlm
import fileinput
import re
MODEL = kenlm.Model('lm-experiment/model/uni-lm5.barpa')
TEMP = ''
# $ pv data/filtered_sentences.txt | parallel -j 2 --keep-order --pipe -L 20 python filter_high_lmscore_uni.py > lm-hscore-sentences.txt
for line in fileinput.input():
    context = re.sub(r'\s+',' ', re.sub(r'([^\u0021-\u007A])', r' \1 ', line)).strip()
    try:
        score = float(MODEL.score(context))
        if score > -15.0: out = '{}\t\t{:.1f}'.format(line[:-1], score)
        else: out = ''
        if out == TEMP: out = ''
    except: out = ''
    
    if len(out) > 0: print(out)
    else:
        if len(TEMP) > 0: print(out) 
    TEMP = out

    